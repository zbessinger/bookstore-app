<?php

// this file must be stored in:
// protected/components/WebUser.php

class WebUser extends CWebUser {

    // Store model to not repeat query.
    private $_model;
    private $_id;
    private $_firstName;
    private $_lastName;
    private $_username;
    private $_type;

    function loadUser() {
        if ($this->_model === NULL) {
            $record = Employee::model()->findByPk($this->id);
            if (!($record === NULL)) {
                $this->_id = $record->id;
                $this->_firstName = $record->firstName;
                $this->_lastName = $record->lastName;
                $this->_username = $record->username;
                $this->_type = $record->type;
            }
            return $this->_model = $record;
        }
//        parent::__construct();
    }

    /**
     * Return Employee ID
     * @return Integer Employee ID
     */
    function getEmployeeId() {
        $this->loadUser();
        return $this->_id;
    }

    /**
     * Return Employee Full Name
     * @return String Employee Full Name
     */
    function getFullName() {
        $this->loadUser();
        return $this->_firstName . " " . $this->_lastName;
    }

    /**
     * Return First Name
     * @return String First Name
     */
    function getFirstName() {
        $this->loadUser();
        return $this->_firstName;
    }

    /**
     * Return Last Name
     * @return String Last Name
     */
    function getLastName() {
        $this->loadUser();
        return $this->_lastName;
    }

    /**
     * Return the username
     * @return String username
     */
    function getUsername() {
        $this->loadUser();
        return $this->_username;
    }

    /**
     * Get the user type
     * @return Integer user type
     */
    function getUserType() {
        $this->loadUser();
        return $this->_type;
    }

    /**
     * Check if the user is admin
     * @return Boolean true if the user is admin, and false otherwise
     */
    function getIsAdmin() {
        $this->loadUser();
        return (intval($this->_type) == 1) ? true : false;
    }

    /**
     * Return the username if the user is admin
     * @return String the username if the user is admin
     */
    function getAdminName() {
        $this->loadUser();
        if ($this->getIsAdmin()){
            return array($this->_username);
        }
        return array('NONE#');
    }

}

?>