<div class="view">
    
	<b><?php echo CHtml::encode($data->getAttributeLabel('authorName')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->lastName ." " . $data->firstName), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('bio')); ?>:</b>
	<?php echo CHtml::encode($data->bio); ?>
	<br />
        
</div>             