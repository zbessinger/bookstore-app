<?php
$this->breadcrumbs=array(
	'Sales'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Sales', 'url'=>array('index')),
	array('label'=>'Sale a Book', 'url'=>array('create')),
	array('label'=>'View a Sale', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage Sales', 'url'=>array('admin')),
);
?>

<h1>Update PurchaseHistory <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>