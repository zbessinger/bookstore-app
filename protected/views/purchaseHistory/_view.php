<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('customerId')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->customer->fullName), array('customer/view','id'=>$data->customerId)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('bookId')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->book->title), array('book/view','id'=>$data->bookId)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('sellerId')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->seller->fullName), array('employee/view','id'=>$data->sellerId)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('quantity')); ?>:</b>
	<?php echo CHtml::encode($data->quantity); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('price')); ?>:</b>
	<?php echo CHtml::encode($data->price); ?>
	<br />


</div>