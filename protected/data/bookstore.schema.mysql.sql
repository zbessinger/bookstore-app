-- phpMyAdmin SQL Dump
-- version 3.4.7.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Dec 03, 2011 at 09:02 PM
-- Server version: 5.1.56
-- PHP Version: 5.2.9

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `tin003_bookstore`
--

-- --------------------------------------------------------

--
-- Table structure for table `authors`
--

CREATE TABLE IF NOT EXISTS `authors` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `firstName` varchar(45) NOT NULL,
  `lastName` varchar(45) NOT NULL,
  `bio` text,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=13 ;

--
-- Dumping data for table `authors`
--

INSERT INTO `authors` (`id`, `firstName`, `lastName`, `bio`) VALUES
(1, 'Abdulrhman', 'Alkhodiry', ''),
(2, 'Zach', 'Bessinger', 'Hi'),
(3, 'Mitchell', 'Aboulafia', ''),
(4, 'Samson', 'Abramsky', ''),
(5, 'Larry', 'Alexander', ''),
(6, 'Hillary ', 'Jordan', 'Hillary Jordan grew up in Texas and Oklahoma. She received her BA in English and Political Science from Wellesley College and spent fifteen years working as an advertising copywriter before starting to write fiction. She got her MFA in Creative Writing from Columbia University.\r\n\r\nHer first novel, MUDBOUND, was published by Algonquin Books in March 2008. It won the 2006 Bellwether Prize for Fiction, founded by Barbara Kingsolver and awarded biennially to an unpublished debut novel that addresses issues of social justice, and a 2009 Alex Award from the American Library Association. It was the 2008 NAIBA (New Atlantic Independent Booksellers Assoc.) Fiction Book of the Year and one of IndieNext''s top ten reading group suggestions. It was a Barnes & Noble Discover Great New Writers pick, a Borders Original Voices selection, a Book Sense pick, and one of twelve New Voices of 2008'),
(7, 'Walter ', 'Isaacson', ''),
(8, 'Jeffrey ', 'Winesett', 'Jeffrey Winesett is director of application development at Control Group, Inc. in New York City. He has been building large-scale web-based applications for over 10 years and has been a champion of the Yii framework since its initial alpha version. He frequently publishes articles on specific Yii topics and uses Yii + PHP whenever possible on development projects.\r\n'),
(9, 'Donald', 'Knuth', 'Donald E. Knuth is known throughout the world for his pioneering work on algorithms and programming techniques, for his invention of the TEX and METAFONT systems for computer typesetting, and for his prolific and influential writing (26 books, 161 papers). Professor Emeritus of The Art of Computer Programming at Stanford University, he currently devotes full time to the completion of his seminal multivolume series on classical computer science, begun in 1962 when he was a graduate student at California Institute of Technology. Professor Knuth is the recipient of numerous awards and honors, including the ACM Turing Award, the Medal of Science presented by President Carter, the AMS Steele Prize for expository writing, and, in November, 1996, the prestigious Kyoto Prize for advanced technology. He lives on the Stanford campus with his wife, Jill. '),
(10, 'Henry', 'Warren', 'Henry S. Warren, Jr., has had a forty-year career with IBM, spanning from the IBM 704 to the PowerPC. He has worked on various military command and control systems and on the SETL project under Jack Schwartz at New York University. Since 1973 he has been with IBM''s Research Division, focusing on compilers and computer architectures. Hank currently works on the Blue Gene petaflop computer project. He received his Ph.D. in computer science from the Courant Institute at New York University.'),
(11, 'Kernigan', 'Ritchie', ''),
(12, 'Joshua', 'Bloch', 'Joshua Bloch is chief Java architect at Google and a Jolt Award winner. He was previously a distinguished engineer at Sun Microsystems and a senior systems designer at Transarc. Bloch led the design and implementation of numerous Java platform features, including JDK 5.0 language enhancements and the award-winning Java Collections Framework. He coauthored Java™ Puzzlers (Addison-Wesley, 2005) and Java™ Concurrency in Practice (Addison-Wesley, 2006).');

-- --------------------------------------------------------

--
-- Table structure for table `books`
--

CREATE TABLE IF NOT EXISTS `books` (
  `id` varchar(13) NOT NULL,
  `title` varchar(150) NOT NULL,
  `authorId` int(10) unsigned NOT NULL,
  `publisherId` int(10) unsigned NOT NULL,
  `year` varchar(4) NOT NULL,
  `genraId` int(5) unsigned NOT NULL,
  `description` text,
  `conditionId` int(5) unsigned NOT NULL,
  `quantity` int(5) NOT NULL DEFAULT '0',
  `price` int(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `fk_books_1` (`authorId`),
  KEY `fk_books_2` (`publisherId`),
  KEY `fk_books_3` (`conditionId`),
  KEY `fk_books_4` (`genraId`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `books`
--

INSERT INTO `books` (`id`, `title`, `authorId`, `publisherId`, `year`, `genraId`, `description`, `conditionId`, `quantity`, `price`) VALUES
('978145164853', 'Steve Jobs', 7, 8, '2011', 2, 'Based on more than forty interviews with Jobs conducted over two years—as well as interviews with more than a hundred family members, friends, adversaries, competitors, and colleagues—Walter Isaacson has written a riveting story of the roller-coaster life and searingly intense personality of a creative entrepreneur whose passion for perfection and ferocious drive revolutionized six industries: personal computers, animated movies, music, phones, tablet computing, and digital publishing.\r\n\r\nAt a time when America is seeking ways to sustain its innovative edge, and when societies around the world are trying to build digital-age economies, Jobs stands as the ultimate icon of inventiveness and applied imagination. He knew that the best way to create value in the twenty-first century was to connect creativity with technology. He built a company where leaps of the imagination were combined with remarkable feats of engineering.  \r\n\r\nAlthough Jobs cooperated with this book, he asked for no control over what was written nor even the right to read it before it was published. He put nothing off-limits. He encouraged the people he knew to speak honestly. And Jobs speaks candidly, sometimes brutally so, about the people he worked with and competed against. His friends, foes, and colleagues provide an unvarnished view of the passions, perfectionism, obsessions, artistry, devilry, and compulsion for control that shaped his approach to business and the innovative products that resulted.\r\n\r\nDriven by demons, Jobs could drive those around him to fury and despair. But his personality and products were interrelated, just as Apple’s hardware and software tended to be, as if part of an integrated system. His tale is instructive and cautionary, filled with lessons about innovation, character, leadership, and values.', 1, 130, 34),
('978156512629', 'When She Woke ', 6, 4, '2011', 1, 'Hannah Payne''s life has been devoted to church and family. But after she''s convicted of murder, she awakens in a new body to a nightmarish new life. She finds herself lying on a table in a bare room, covered only by a paper gown, with cameras broadcasting her every move to millions at home, for whom observing new Chromes--criminals whose skin color has been genetically altered to match the class of their crime--is a sinister form of entertainment. Hannah is a Red for the crime of murder. The victim, says the State of Texas, was her unborn child, and Hannah is determined to protect the identity of the father, a public figure with whom she shared a fierce and forbidden love.\r\nA powerful reimagining of The Scarlet Letter, When She Woke is a timely fable about a stigmatized woman struggling to navigate an America of the not-too-distant future, where the line between church and state has been eradicated, and convicted felons are no longer imprisoned and rehabilitated but chromed and released back into the population to survive as best they can. In seeking a path to safety in an alien and hostile world, Hannah unknowingly embarks on a journey of self-discovery that forces her to question the values she once held true and the righteousness of a country that politicizes faith and love. ', 1, 30, 15),
('978156512677', 'Mudbound', 6, 4, '2010', 7, 'In Jordan''s prize-winning debut, prejudice takes many forms, both subtle and brutal. It is 1946, and city-bred Laura McAllan is trying to raise her children on her husband''s Mississippi Delta farm—a place she finds foreign and frightening. In the midst of the family''s struggles, two young men return from the war to work the land. Jamie McAllan, Laura''s brother-in-law, is everything her husband is not—charming, handsome, and haunted by his memories of combat. Ronsel Jackson, eldest son of the black sharecroppers who live on the McAllan farm, has come home with the shine of a war hero. But no matter his bravery in defense of his country, he is still considered less than a man in the Jim Crow South. It is the unlikely friendship of these brothers-in-arms that drives this powerful novel to its inexorable conclusion.\r\n\r\nThe men and women of each family relate their versions of events and we are drawn into their lives as they become players in a tragedy on the grandest scale. As Kingsolver says of Hillary Jordan, "Her characters walked straight out of 1940s Mississippi and into the part of my brain where sympathy and anger and love reside, leaving my heart racing. They are with me still."', 1, 10, 22),
('978184719958', 'Agile Web Application Development with Yii 1.1 and PHP5', 8, 9, '2011', 19, 'This is a step-by-step tutorial for developing web applications using Yii. This book follows the test-first, incremental, and iterative approach to software development while developing a project task management application called "TrackStar". If you are a PHP programmer with knowledge of object oriented programming and want to rapidly develop modern, sophisticated web applications, then this book is for you. No prior knowledge of Yii is required to read this book.', 1, 12, 34),
('9780321751041', 'The Art of Computer Programming, Volumes 1-4A Boxed Set', 9, 10, '2011', 27, 'This last volume in a set of three surveys classical computer techniques for sorting and searching, considering both large and small databases and internal and external memories. Recently updated to reflect new developments in the field, the volume also contains several hundred new exercises. This new edition of Volume Three is described as matching Volume One (3rd edition) covering fundamental algorithms and Volume Two (3rd edition) treating seminumerical algorithms. Annotation c. by Book News, Inc., Portland, Or.', 1, 1, 200),
('978020191465', 'Hacker''s Delight', 10, 10, '2002', 27, 'A collection useful programming advice the author has collected over the years; small algorithms that make the programmer''s task easier. * At long last, proven short-cuts to mastering difficult aspects of computer programming * Learn to program at a more advanced level than is generally taught in schools and training courses, and much more advanced than can be learned through individual study/experience. * An instant cult classic for programmers! Computer programmers are often referred to as hackers -- solitary problem solvers engrossed in a world of code as they seek elegant solutions to building better software. While many view these unique individuals as "madmen," the truth is that much of the computer programmer''s job involves a healthy mix of arithmetic and logic. In Hacker''s Delight, veteran programmer Hank Warren shares the collected wisdom -- namely tips and tricks -- from his considerable experience in the world of application development. The resulting work is an irresistible collection that will help even the most seasoned programmers better their craft. Henry S. Warren Jr. has had a 40-year career with IBM, spanning the computer field from the IBM 704 to PowerPC. He has worked on various military command and control systems, and on the SETL project under Jack Schwartz at NYU. Since 1973 he has been in IBM''s Research Division at Yorktown Heights, New York. Here he has done compiler and computer architecture work on the 801 computer and its several variants through PowerPC. Presently he is working on the Blue Gene petaflop computer project. He received his Ph.D. in Computer Science from the Courant Institute at New York University in 1980.', 1, 5, 40),
('978013110362', 'C Programming Language', 11, 11, '1988', 27, 'The authors present the complete guide to ANSI standard C language programming. Written by the developers of C, this new version helps readers keep up with the finalized ANSI standard for C while showing how to take advantage of C''s rich set of operators, economy of expression, improved control flow, and data structures. The 2/E has been completely rewritten with additional examples and problem sets to clarify the implementation of difficult language constructs. For years, C programmers have let K&R guide them to building well-structured and efficient programs. Now this same help is available to those working with ANSI compilers. Includes detailed coverage of the C language plus the official C language reference manual for at-a-glance help with syntax notation, declarations, ANSI changes, scope rules, and the list goes on and on. ', 1, 10, 60),
('978032135668', 'Effective Java', 12, 10, '2008', 27, 'Are you looking for a deeper understanding of the Java™ programming language so that you can write code that is clearer, more correct, more robust, and more reusable? Look no further! Effective Java™, Second Edition, brings together seventy-eight indispensable programmer’s rules of thumb: working, best-practice solutions for the programming challenges you encounter every day.\r\n\r\n \r\n\r\nThis highly anticipated new edition of the classic, Jolt Award-winning work has been thoroughly updated to cover Java SE 5 and Java SE 6 features introduced since the first edition. Bloch explores new design patterns and language idioms, showing you how to make the most of features ranging from generics to enums, annotations to autoboxing.\r\n\r\n \r\n\r\nEach chapter in the book consists of several “items” presented in the form of a short, standalone essay that provides specific advice, insight into Java platform subtleties, and outstanding code examples. The comprehensive descriptions and explanations for each item illuminate what to do, what not to do, and why.\r\n\r\n \r\n\r\nHighlights include:\r\n\r\n    New coverage of generics, enums, annotations, autoboxing, the for-each loop, varargs, concurrency utilities, and much more\r\n    Updated techniques and best practices on classic topics, including objects, classes, libraries, methods, and serialization\r\n    How to avoid the traps and pitfalls of commonly misunderstood subtleties of the language\r\n    Focus on the language and its most fundamental libraries: java.lang, java.util, and, to a lesser extent, java.util.concurrent and java.io\r\n\r\nSimply put, Effective Java™, Second Edition, presents the most practical, authoritative guidelines available for writing efficient, well-designed programs.', 1, 3, 50);

-- --------------------------------------------------------

--
-- Table structure for table `conditions`
--

CREATE TABLE IF NOT EXISTS `conditions` (
  `id` int(5) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `description` tinytext NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name_UNIQUE` (`name`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `conditions`
--

INSERT INTO `conditions` (`id`, `name`, `description`) VALUES
(1, 'New', 'Just like it sounds. A brand-new, unused, unread copy in perfect condition.'),
(2, 'Like New', 'An apparently unread copy in perfect condition. Dust cover is intact, with no nicks or tears. Spine has no signs of creasing. Pages are clean and are not marred by notes or folds of any kind. Book may contain a remainder mark on an outside edge.'),
(3, 'Very Good', 'A copy that has been read, but remains in excellent condition. Pages are intact and are not marred by notes or highlighting. The spine remains undamaged. '),
(4, 'Good', 'A copy that has been read, but remains in clean condition. All pages are intact, and the cover is intact (including dust cover, if applicable). The spine may show signs of wear. Pages can include limited notes and highlighting.'),
(5, 'Acceptable', 'A readable copy. All pages are intact, and the cover is intact (the dust cover may be missing). Pages can include considerable notes--in pen or highlighter--but the notes cannot obscure the text.'),
(6, 'Unacceptable', 'Moldy, badly stained, or unclean copies are not acceptable, nor are copies with missing pages or obscured text. Books that are distributed for promotional use only are prohibited. This includes advance reading copies (ARCs) and uncorrected proof copies.');

-- --------------------------------------------------------

--
-- Table structure for table `customers`
--

CREATE TABLE IF NOT EXISTS `customers` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `firstName` varchar(45) NOT NULL,
  `lastName` varchar(45) NOT NULL,
  `address` tinytext NOT NULL,
  `phoneNumber` varchar(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `customers`
--

INSERT INTO `customers` (`id`, `firstName`, `lastName`, `address`, `phoneNumber`) VALUES
(1, 'Abdulrhman', 'Alkhodiry', 'WHAT', '2709962799'),
(2, 'Ahmed', 'Alsalama', 'None', '2707927888');

-- --------------------------------------------------------

--
-- Table structure for table `employees`
--

CREATE TABLE IF NOT EXISTS `employees` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `firstName` varchar(45) NOT NULL,
  `lastName` varchar(45) NOT NULL,
  `username` varchar(45) NOT NULL,
  `password` varchar(180) NOT NULL,
  `type` int(1) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `employees`
--

INSERT INTO `employees` (`id`, `firstName`, `lastName`, `username`, `password`, `type`) VALUES
(1, 'Abdulrhman', 'Alkhodiry', 'zeroows', '$P$BTrAZhCq6bROn9LgmntkTO3j3dQJtk.', 1),
(3, 'Demo', 'Demo', 'demo', '$P$BFy8Bmw4.7d7d8fiKiPmOo2f3K4A/80', 2),
(4, 'Zack', 'Bessinger', 'Zach', '$P$B46fx7mZoWy2K2kyowHBEesWbN2XlM.', 1);

-- --------------------------------------------------------

--
-- Table structure for table `genres`
--

CREATE TABLE IF NOT EXISTS `genres` (
  `id` int(5) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(150) NOT NULL,
  `description` tinytext,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=28 ;

--
-- Dumping data for table `genres`
--

INSERT INTO `genres` (`id`, `name`, `description`) VALUES
(1, 'Art and design ', 'Art and design '),
(2, 'Biography ', 'Biography '),
(3, 'Business and finance ', 'Business and finance '),
(4, 'Classics ', 'Classics '),
(5, 'Comics and graphic novels', 'Comics and graphic novels'),
(6, 'Crime fiction ', 'Crime fiction '),
(7, 'Fantasy', 'Fantasy'),
(8, 'Fiction', 'Fiction'),
(9, 'Food and drink ', 'Food and drink '),
(10, 'Health, mind and body', 'Health, mind and body'),
(11, 'History ', 'History '),
(12, 'Horror ', 'Horror '),
(13, 'House and garden ', 'House and garden '),
(14, 'Philosophy ', 'Philosophy '),
(15, 'Poetry ', 'Poetry '),
(19, 'Reference and languages', 'Reference and languages'),
(20, 'Religion', 'Religion'),
(21, 'Romance ', 'Romance '),
(22, 'Science and nature ', 'Science and nature '),
(23, 'Science fiction ', 'Science fiction '),
(24, 'Society ', 'Society '),
(25, 'Thrillers', 'Thrillers'),
(26, 'Travel guides', 'Travel guides'),
(27, 'Computers', 'Computers');

-- --------------------------------------------------------

--
-- Table structure for table `publishers`
--

CREATE TABLE IF NOT EXISTS `publishers` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(70) NOT NULL,
  `address` tinytext,
  `website` varchar(80) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name_UNIQUE` (`name`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=12 ;

--
-- Dumping data for table `publishers`
--

INSERT INTO `publishers` (`id`, `name`, `address`, `website`) VALUES
(1, 'WKU', '', ''),
(2, 'Aberdeen Bay', 'Jill Cline, Acquisition Director, 5109 Eaton Rapids Road, Albion MI 49224; 703-346-6547. Publish mainstream novels, mysteries, and memoirs.', 'http://www.aberdeenbay.com'),
(3, 'Abingdon Press', 'Ramona Richards, Acquisitions Editor, Fiction, 201 Eighth Avenue South, P O Box 801, Nashville TN 37202-0801; 615-749-6792.', 'http://www.abingdonpress.com'),
(4, 'Ace Books', 'Anne Sowards, Editor, 375 Hudson Street, New York NY 10014; 212-366-2000; Fax: 212-366-2385.', 'http://www.penguinputnam.com'),
(5, 'Algonquin Press', 'Shannon Ravenel, Editor, Shannon Ravenel Books, 127 Kingston Drive #105, Chapel Hill NC 27514; 919-967-0108; Fax: 919-933-0272. Email: shannon@algonquin.com.', 'http://www.algonquin.com/'),
(8, 'Amistad Press', 'Dawn Davis, Editorial Director, HarperCollins, 10 East 53rd Street, New York NY 10022; 212-207-7541; Fax: 212-207-6927.', 'http://www.harpercollins.com'),
(9, 'Atria', 'Judith Curr, Editor, Pocket Books, 1230 Avenue of the Americas, New York NY 10020-1513.', 'http://www.simonandschuster.com'),
(10, 'Addison-Wesley', '', ''),
(11, 'Prentice Hall', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `purchasesHistory`
--

CREATE TABLE IF NOT EXISTS `purchasesHistory` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `customerId` int(10) unsigned NOT NULL,
  `bookId` varchar(13) NOT NULL,
  `sellerId` int(10) unsigned NOT NULL,
  `quantity` int(5) unsigned NOT NULL,
  `price` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_purchasesHistory_1` (`bookId`),
  KEY `fk_purchasesHistory_2` (`customerId`),
  KEY `fk_purchasesHistory_3` (`sellerId`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `purchasesHistory`
--

INSERT INTO `purchasesHistory` (`id`, `customerId`, `bookId`, `sellerId`, `quantity`, `price`) VALUES
(1, 1, '978145164853', 1, 1, 24),
(2, 2, '978145164853', 4, 1, 30);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
